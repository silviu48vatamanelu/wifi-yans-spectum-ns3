Setting up the network...
Using SpectrumWifiPhyHelper for the network and two APs in the network
Channel Setup
Network Setup complete
Mobility Setup complete
Address Setup complete
Applications Setup complete
Example Setup complete
Simulation time: 1684 seconds
 ----- 1 -----

From 192.168.1.2 To 192.168.1.1

Tx Packets: 180000

Rx Packets: 179934

Throughput: 184.252 Mbps
Lost Packets: 66

Packet Loss Ratio: 0.000366667

Delay Sum: 1430.76

RTT: 0.0079516

 ----- 2 -----

From 192.168.2.2 To 192.168.2.1

Tx Packets: 180000

Rx Packets: 179898

Throughput: 184.216 Mbps
Lost Packets: 102

Packet Loss Ratio: 0.000566667

Delay Sum: 1556.46

RTT: 0.00865189

 ----- 3 -----

From 192.168.1.1 To 192.168.1.2

Tx Packets: 179934

Rx Packets: 179880

Throughput: 184.197 Mbps
Lost Packets: 53

Packet Loss Ratio: 0.000294552

Delay Sum: 2347.8

RTT: 0.013052

 ----- 4 -----

From 192.168.2.1 To 192.168.2.2

Tx Packets: 179898

Rx Packets: 179739

Throughput: 184.053 Mbps
Lost Packets: 157

Packet Loss Ratio: 0.000872717

Delay Sum: 2123.5

RTT: 0.0118144

