Setting up the network...
Using YansWifiPhyHelper for the network and two APs in the network
Channel Setup
Network Setup complete
Mobility Setup complete
Address Setup complete
Applications Setup complete
Example Setup complete
Simulation time: 1769 seconds
 ----- 1 -----

From 192.168.1.2 To 192.168.1.1

Tx Packets: 180000

Rx Packets: 179924

Throughput: 184.242 Mbps
Lost Packets: 71

Packet Loss Ratio: 0.000394444

Delay Sum: 88.6641

RTT: 0.000492786

 ----- 2 -----

From 192.168.2.2 To 192.168.2.1

Tx Packets: 180000

Rx Packets: 179891

Throughput: 184.208 Mbps
Lost Packets: 106

Packet Loss Ratio: 0.000588889

Delay Sum: 88.2149

RTT: 0.000490379

 ----- 3 -----

From 192.168.1.1 To 192.168.1.2

Tx Packets: 179924

Rx Packets: 179875

Throughput: 184.192 Mbps
Lost Packets: 49

Packet Loss Ratio: 0.000272337

Delay Sum: 118.872

RTT: 0.00066086

 ----- 4 -----

From 192.168.2.1 To 192.168.2.2

Tx Packets: 179891

Rx Packets: 179777

Throughput: 184.092 Mbps
Lost Packets: 114

Packet Loss Ratio: 0.000633717

Delay Sum: 119.777

RTT: 0.000666252

