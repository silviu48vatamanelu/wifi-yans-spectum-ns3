#include "ns3/applications-module.h"
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/csma-module.h"
#include "ns3/double.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/log.h"
#include "ns3/mobility-helper.h"
#include "ns3/mobility-model.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/nist-error-rate-model.h"
#include "ns3/nix-vector-helper.h"
#include "ns3/spectrum-analyzer-helper.h"
#include "ns3/spectrum-channel.h"
#include "ns3/spectrum-helper.h"
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/string.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/udp-server.h"
#include "ns3/uinteger.h"
#include "ns3/wifi-helper.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"

#include <chrono>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("WifiAssignment");

double g_signalDbmAvg; //!< Average signal power [dBm]
double g_noiseDbmAvg;  //!< Average noise power [dBm]
uint32_t g_samples;    //!< Number of samples

/**
 * Monitor sniffer Rx trace
 *
 * \param packet The sensed packet.
 * \param channelFreqMhz The channel frequency [MHz].
 * \param txVector The Tx vector.
 * \param aMpdu The aMPDU.
 * \param signalNoise The signal and noise dBm.
 * \param staId The STA ID.
 */
void
MonitorSniffRx(Ptr<const Packet> packet,
               uint16_t channelFreqMhz,
               WifiTxVector txVector,
               MpduInfo aMpdu,
               SignalNoiseDbm signalNoise,
               uint16_t staId)

{
    g_samples++;
    g_signalDbmAvg += ((signalNoise.signal - g_signalDbmAvg) / g_samples);
    g_noiseDbmAvg += ((signalNoise.noise - g_noiseDbmAvg) / g_samples);
}

//   STA                  AP
//  (0,0,0)            (0,5,0)
//    n1 ---------------- n2    (if doubleAp is false)
//                        |
//                        |-- n5 (2.5, 7.5, 0)server (if server is true)
//                        |
//    n3 ---------------- n4    (if doubleAp is true)
//  (5,0,0)            (5,5,0)
//

int
main(int argc, char* argv[])
{
    LogComponentEnable("WifiAssignment", LOG_LEVEL_INFO);

    Time simulationTime{"20s"};
    // uint32_t payloadSize = 100;
    std::string errorModelType{"ns3::NistErrorRateModel"};
    bool verbose{false};
    bool yans{false};
    bool spectrum{false};
    bool doubleAp{false};
    bool interference{false};
    bool server{false};
    bool time{false};

    CommandLine cmd(__FILE__);
    cmd.AddValue("yans", "Use the YansWifiPhyHelper", yans);
    cmd.AddValue("spectrum", "Use the SpectrumWifiPhyHelper", spectrum);
    cmd.AddValue("verbose", "turn on all WifiNetDevice log components", verbose);
    cmd.AddValue("doubleAp", "Use a single AP", doubleAp);
    cmd.AddValue("interference", "Enable interference", interference);
    cmd.AddValue("server", "Use a single AP", server);
    cmd.AddValue("time", "Calculate simulation time", time);
    cmd.Parse(argc, argv);

    NS_LOG_INFO("Setting up the network...");
    NS_LOG_INFO("Using " << (yans ? "YansWifiPhyHelper" : "SpectrumWifiPhyHelper")
                         << " for the network and " << (!doubleAp ? "a single AP" : "two APs")
                         << " in the network");

    NodeContainer firstNetwork;
    firstNetwork.Create(2);

    NodeContainer secondNetwork;
    secondNetwork.Create(2);

    // The below set of helpers will help us to put together the wifi NICs we want
    WifiHelper wifi;
    if (verbose)
    {
        WifiHelper::EnableLogComponents(); // Turn on all Wifi logging
    }
    wifi.SetStandard(WIFI_STANDARD_80211n);

    if (yans == spectrum)
    {
        std::cout << "You must choose between yans and spectrum" << std::endl;
        return 1;
    }

    int nodes = (!doubleAp) ? 1 : 2;

    NodeContainer wifiStaNode;
    wifiStaNode.Create(nodes);
    NodeContainer wifiApNode;
    wifiApNode.Create(nodes);
    NodeContainer serverContainer;
    NodeContainer csmaContainer;

    YansWifiPhyHelper yansPhy;
    SpectrumWifiPhyHelper spectrumPhy;
    if (yans)
    {
        YansWifiChannelHelper channel = YansWifiChannelHelper::Default();
        channel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
        yansPhy.SetChannel(channel.Create());
    }
    else if (spectrum)
    {
        Ptr<MultiModelSpectrumChannel> spectrumChannel = CreateObject<MultiModelSpectrumChannel>();
        Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
        spectrumChannel->AddPropagationLossModel(lossModel);

        Ptr<ConstantSpeedPropagationDelayModel> delayModel =
            CreateObject<ConstantSpeedPropagationDelayModel>();
        spectrumChannel->SetPropagationDelayModel(delayModel);

        spectrumPhy.SetChannel(spectrumChannel);
        spectrumPhy.SetErrorRateModel(errorModelType);
    }
    NS_LOG_INFO("Channel Setup");

    WifiMacHelper mac;

    Ssid ssid = Ssid("first-network");
    NetDeviceContainer staDevice;
    NetDeviceContainer apDevice;

    if (yans)
    {
        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
        yansPhy.Set("ChannelSettings", StringValue(std::string("{1, 20, BAND_2_4GHZ, 0}")));
        staDevice.Add(wifi.Install(yansPhy, mac, wifiStaNode.Get(0)));
        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
        apDevice.Add(wifi.Install(yansPhy, mac, wifiApNode.Get(0)));

        if (doubleAp)
        {
            ssid = Ssid("second-network");
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
            if (interference)
            {
                yansPhy.Set("ChannelSettings", StringValue(std::string("{1, 20, BAND_2_4GHZ, 0}")));
            }
            else
            {
                yansPhy.Set("ChannelSettings", StringValue(std::string("{5, 20, BAND_2_4GHZ, 0}")));
            }
            staDevice.Add(wifi.Install(yansPhy, mac, wifiStaNode.Get(1)));
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
            apDevice.Add(wifi.Install(yansPhy, mac, wifiApNode.Get(1)));
        }
    }
    else if (spectrum)
    {
        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
        spectrumPhy.Set("ChannelSettings", StringValue(std::string("{1, 20, BAND_2_4GHZ, 0}")));
        staDevice.Add(wifi.Install(spectrumPhy, mac, wifiStaNode.Get(0)));
        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
        apDevice.Add(wifi.Install(spectrumPhy, mac, wifiApNode.Get(0)));

        if (doubleAp)
        {
            ssid = Ssid("second-network");
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
            if (interference)
            {
                spectrumPhy.Set("ChannelSettings",
                                StringValue(std::string("{1, 20, BAND_2_4GHZ, 0}")));
            }
            else
            {
                spectrumPhy.Set("ChannelSettings",
                                StringValue(std::string("{5, 20, BAND_2_4GHZ, 0}")));
            }
            staDevice.Add(wifi.Install(spectrumPhy, mac, wifiStaNode.Get(1)));
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
            apDevice.Add(wifi.Install(spectrumPhy, mac, wifiApNode.Get(1)));
        }
    }

    CsmaHelper csma;
    NetDeviceContainer csmaDevices;
    if (server)
    {
        serverContainer.Create(1);

        csmaContainer.Add(wifiApNode.Get(0));
        if (doubleAp)
        {
            csmaContainer.Add(wifiApNode.Get(1));
        }
        csmaContainer.Add(serverContainer.Get(0));

        csma.SetChannelAttribute("DataRate", DataRateValue(DataRate(10 * 10e9)));
        csma.SetChannelAttribute("Delay", TimeValue(NanoSeconds(6560)));

        csmaDevices = csma.Install(csmaContainer);
    }
    NS_LOG_INFO("Network Setup complete");
    // mobility.
    MobilityHelper mobility;
    Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();

    // AP locations
    positionAlloc->Add(Vector(0.0, 5.0, 0.0));
    if (doubleAp)
    {
        positionAlloc->Add(Vector(5.0, 5.0, 0.0));
    }

    // STA locations
    positionAlloc->Add(Vector(0.0, 0.0, 0.0));
    if (doubleAp)
    {
        positionAlloc->Add(Vector(5.0, 0.0, 0.0));
    }

    if (server)
    {
        serverContainer.Create(1);
        positionAlloc->Add(Vector(2.5, 7.5, 0.0));
    }

    mobility.SetPositionAllocator(positionAlloc);
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

    mobility.Install(wifiApNode);
    mobility.Install(wifiStaNode);

    if (server)
    {
        mobility.Install(serverContainer);
    }
    NS_LOG_INFO("Mobility Setup complete");
    /* Internet stack*/
    InternetStackHelper stack;
    Ipv4NixVectorHelper routingHelper;
    stack.SetRoutingHelper(routingHelper);
    stack.Install(wifiApNode);
    stack.Install(wifiStaNode);
    stack.Install(serverContainer);

    Ipv4AddressHelper address;
    address.SetBase("192.168.1.0", "255.255.255.0");
    Ipv4InterfaceContainer firstStaNodeInterface, secondStaNodeInterface;
    Ipv4InterfaceContainer firstApNodeInterface, secondApNodeInterface;
    Ipv4InterfaceContainer csmaInterfaces;

    firstStaNodeInterface = address.Assign(staDevice.Get(0));
    firstApNodeInterface = address.Assign(apDevice.Get(0));

    if (doubleAp)
    {
        address.SetBase("192.168.2.0", "255.255.255.0");

        secondStaNodeInterface = address.Assign(staDevice.Get(1));
        secondApNodeInterface = address.Assign(apDevice.Get(1));
    }

    if (server)
    {
        address.SetBase("192.168.3.0", "255.255.255.0");

        csmaInterfaces = address.Assign(csmaDevices);
    }
    NS_LOG_INFO("Address Setup complete");
    /* Setting applications */
    ApplicationContainer firstServerApp, secondServerApp;
    uint16_t port = 9;
    UdpEchoServerHelper echoServer(port);
    if (!doubleAp)
    {
        firstServerApp = echoServer.Install(wifiStaNode.Get(0));
        firstServerApp.Start(Seconds(0.0));
        firstServerApp.Stop(simulationTime + Seconds(1.0));
    }
    else if (doubleAp && !server)
    {
        firstServerApp = echoServer.Install(wifiStaNode.Get(0));
        firstServerApp.Start(Seconds(0.0));
        firstServerApp.Stop(simulationTime + Seconds(1.0));

        secondServerApp = echoServer.Install(wifiStaNode.Get(1));
        secondServerApp.Start(Seconds(0.0));
        secondServerApp.Stop(simulationTime + Seconds(1.0));
    }
    else if (doubleAp && server)
    {
        firstServerApp = echoServer.Install(csmaContainer.Get(2));
        firstServerApp.Start(Seconds(0.0));
        firstServerApp.Stop(simulationTime + Seconds(1.0));
    }
    else
    {
        std::cout << "You shouldn't be here" << std::endl;
        return 1;
    }

    const auto packetInterval = 0.0001;

    UdpEchoClientHelper firstClient((server) ? csmaInterfaces.GetAddress(2)
                                             : firstStaNodeInterface.GetAddress(0),
                                    port);
    firstClient.SetAttribute("MaxPackets", UintegerValue(0));
    firstClient.SetAttribute("Interval", TimeValue(Seconds(packetInterval)));
    firstClient.SetAttribute("PacketSize", UintegerValue(100));
    ApplicationContainer firstClientApp =
        firstClient.Install((server) ? wifiStaNode.Get(0) : wifiApNode.Get(0));
    firstClientApp.Start(Seconds(1.0));
    firstClientApp.Stop(simulationTime - Seconds(1.0));

    if (doubleAp)
    {
        UdpEchoClientHelper secondClient((server) ? csmaInterfaces.GetAddress(2)
                                                  : secondStaNodeInterface.GetAddress(0),
                                         port);
        secondClient.SetAttribute("MaxPackets", UintegerValue(0));
        secondClient.SetAttribute("Interval", TimeValue(Seconds(packetInterval)));
        secondClient.SetAttribute("PacketSize", UintegerValue(100));
        ApplicationContainer secondClientApp =
            secondClient.Install((server) ? wifiStaNode.Get(1) : wifiApNode.Get(1));
        secondClientApp.Start(Seconds(1.0));
        secondClientApp.Stop(simulationTime - Seconds(1.0));
    }

    NS_LOG_INFO("Applications Setup complete");

    Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/Phy/MonitorSnifferRx",
                                  MakeCallback(&MonitorSniffRx));

    if (doubleAp)
    {
        Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/Phy/MonitorSnifferRx",
                                      MakeCallback(&MonitorSniffRx));
    }

    if (yans)
    {
        yansPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
        std::stringstream ss;
        ss << "wifi-wifi-anche-stipendio-yans";
        yansPhy.EnablePcap(ss.str(), apDevice);
    }
    else if (spectrum)
    {
        spectrumPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
        std::stringstream ss;
        ss << "wifi-wifi-anche-stipendio-spectrum";
        spectrumPhy.EnablePcap(ss.str(), apDevice);
    }

    g_signalDbmAvg = 0;
    g_noiseDbmAvg = 0;
    g_samples = 0;
    NS_LOG_INFO("Example Setup complete");
    Ptr<FlowMonitor> flowMonitor;
    FlowMonitorHelper flowHelper;
    flowMonitor = flowHelper.InstallAll();

    Simulator::Stop(simulationTime + Seconds(1.0));

    if (time)
    {
        auto startTime = std::chrono::high_resolution_clock::now();
        Simulator::Run();
        auto stopTime = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stopTime - startTime);
        NS_LOG_INFO("Simulation time: " << duration.count() / 1000 << " seconds");
    }
    else
    {
        Simulator::Run();
    }

    flowMonitor->SerializeToXmlFile("wifi.xml", true, true);
    // Data collection and nice plots
    flowMonitor->CheckForLostPackets();

    Ptr<Ipv4FlowClassifier> classifier =
        DynamicCast<Ipv4FlowClassifier>(flowHelper.GetClassifier());
    auto stats = flowMonitor->GetFlowStats();
    for (auto i = stats.begin(); i != stats.end(); ++i)
    {
        Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow(i->first);
        NS_LOG_INFO(" ----- " << i->first << " -----" << std::endl);
        NS_LOG_INFO("From " << t.sourceAddress << " To " << t.destinationAddress << std::endl);
        NS_LOG_INFO("Tx Packets: " << i->second.txPackets << std::endl);
        NS_LOG_INFO("Rx Packets: " << i->second.rxPackets << std::endl);
        NS_LOG_INFO("Throughput: " << i->second.rxBytes * 8.0 / 1000 / 1000 << " Mbps");
        NS_LOG_INFO("Lost Packets: " << i->second.lostPackets << std::endl);
        NS_LOG_INFO("Packet Loss Ratio: "
                    << ((double)i->second.lostPackets / (double)(i->second.txPackets))
                    << std::endl);
        NS_LOG_INFO("Delay Sum: " << i->second.delaySum.GetSeconds() << std::endl);
        NS_LOG_INFO("RTT: " << i->second.delaySum.GetSeconds() / i->second.rxPackets << std::endl);
    }

    Simulator::Destroy();

    return 0;
}
